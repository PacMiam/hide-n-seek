Hide 'n' Seek Safari
====================

License GPLv3

## Dependencies

 * python3
 * python3-constraint
 * python3-pygame

## Resources

 * Animals icon by [PacMiam](https://pacmiam.tuxfamily.org/) - License ArtLibre
 * 8bit Bossa by [Joth](https://opengameart.org/content/bossa-nova) - License CC0 Public Domain
