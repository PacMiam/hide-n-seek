# ------------------------------------------------------------------------------
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
#   Modules - System
# ------------------------------------------------------------------------------

# Collections
from collections import OrderedDict

# Filesystem
from os.path import basename
from os.path import splitext
from os.path import join as path_join

from glob import glob

# System
from sys import exit as sys_exit

# ------------------------------------------------------------------------------
#   Modules - Engine
# ------------------------------------------------------------------------------

from ui import Data

from lib import Data as DataSolver
from lib.solver import Solver

# ------------------------------------------------------------------------------
#   Modules - SDL
# ------------------------------------------------------------------------------

try:
    import pygame
    import pygame.freetype

    from pygame.locals import *

except ImportError as error:
    sys_exit("Cannot import python3-pygame module: %s" % str(error))

# ------------------------------------------------------------------------------
#   Class
# ------------------------------------------------------------------------------

class Engine(object):

    FPS = 30

    BOARD = (255, 222, 160)
    SIDEBAR = (255, 255, 255)
    BACKGROUND = (222, 180, 102)

    GOOD = (111, 193, 111)
    WARNING = (247, 113, 104)

    FONT = (73, 63, 56)

    def __init__(self, arguments, fullscreen=False):
        """ Constructor

        Parameters
        ----------
        arguments : Namespace
            Application parameters

        Others Parameters
        -----------------
        fullscreen : bool
            Window fullscreen mode (Default: False)

        Raises
        ------
        TypeError
            When fullscreen type is not bool
        """

        # ----------------------------------------
        #   Exceptions
        # ----------------------------------------

        if type(fullscreen) is not bool:
            raise TypeError("Wrong type for fullscreen, expected bool")

        # ----------------------------------------
        #   Variables
        # ----------------------------------------

        self.__title = Data.NAME

        self.__size = (Data.Pixel.SCREEN_WIDTH, Data.Pixel.SCREEN_HEIGHT)

        self.__fullscreen = fullscreen

        # Main loop
        self.__is_alive = True
        self.__need_to_update = True

        # Surface storage
        self.__images = dict()

        # Sound
        self.__mute = arguments.mute
        self.__volume = arguments.volume

        # Mouse
        self.__mouse_drag_mode = False

        # Store board position
        self.__boards_position = OrderedDict({
            Data.Board.TOPLEFT: None,
            Data.Board.TOPRIGHT: None,
            Data.Board.BOTTOMLEFT: None,
            Data.Board.BOTTOMRIGHT: None,
        })

        # Store pieces position
        self.__pieces_position = {
            Data.Piece.FIRST: None,
            Data.Piece.SECOND: None,
            Data.Piece.THIRD: None,
            Data.Piece.FOURTH: None,
        }

        # Store sidebar position
        self.__sidebar_position = {
            Data.Animal.LION: None,
            Data.Animal.ZEBRA: None,
            Data.Animal.ANTELOPE: None,
            Data.Animal.ELEPHANT: None,
            Data.Animal.RHINOCEROS: None
        }

        # Store buttons position
        self.__buttons_position = OrderedDict({
            "resolve": {
                "text": "Resolve - F12",
                "method": self.__on_auto_resolve_boards,
                "position": None,
            },
            "check": {
                "text": "Check - F11",
                "method": self.__on_check_boards,
                "position": None,
            }
        })

        # Store volume position
        self.__volume_position = None

        # Store pieces position and rotation
        self.__pieces = {
            Data.Piece.FIRST: {
                "rotation": Data.Rotation.NORTH,
                "position": None,
            },
            Data.Piece.SECOND: {
                "rotation": Data.Rotation.NORTH,
                "position": None,
            },
            Data.Piece.THIRD: {
                "rotation": Data.Rotation.NORTH,
                "position": None,
            },
            Data.Piece.FOURTH: {
                "rotation": Data.Rotation.NORTH,
                "position": None,
            },
        }

        # Current selected piece
        self.__selected_piece = None
        self.__selected_piece_rotation = Data.Rotation.NORTH

        # Store animals values from specified arguments
        self.__animals = OrderedDict({
            Data.Animal.LION: arguments.l,
            Data.Animal.ZEBRA: arguments.z,
            Data.Animal.ANTELOPE: arguments.a,
            Data.Animal.ELEPHANT: arguments.e,
            Data.Animal.RHINOCEROS: arguments.r
        })

        # Warning message
        self.__message = None
        self.__message_color = self.BOARD

        # ----------------------------------------
        #   Initialization
        # ----------------------------------------

        self.__init_pygame()


    def __str__(self):
        """ Represents current object as string

        Returns
        -------
        str
            Object as string
        """

        data = list()

        data.append("%-8s %s" % (
            "SDL", '.'.join(str(i) for i in pygame.get_sdl_version())))
        data.append("%-8s %s" % (
            "PyGame", pygame.version.ver))
        data.append("%-8s %s" % (
            "Driver", pygame.display.get_driver()))

        return '\n'.join(data)


    # ----------------------------------------
    #   Methods
    # ----------------------------------------

    def __init_pygame(self):
        """ Initialize engine
        """

        # Initialize pygame modules
        pygame.font.init()
        pygame.mixer.init()
        pygame.display.init()
        pygame.freetype.init()

        # ----------------------------------------
        #   Window
        # ----------------------------------------

        self.__screen = pygame.display.set_mode(self.__size)

        pygame.display.set_caption(self.__title)

        # ----------------------------------------
        #   Surface
        # ----------------------------------------

        self.__background = pygame.display.get_surface()

        # ----------------------------------------
        #   Font
        # ----------------------------------------

        self.__font = pygame.font.Font(None, Data.Pixel.CURSOR)

        self.__font_half = pygame.font.Font(None, Data.Pixel.HALF)

        self.__font_medium = pygame.font.Font(None, Data.Pixel.MEDIUM)

        # ----------------------------------------
        #   Clock timer
        # ----------------------------------------

        self.__clock = pygame.time.Clock()

        # ----------------------------------------
        #   Resources - Music
        # ----------------------------------------

        pygame.mixer.music.load(
            path_join("ui", "data", "music", "8bit_bossa.mp3"))

        pygame.mixer.music.set_volume(self.__volume)

        # ----------------------------------------
        #   Resources - Pieces
        # ----------------------------------------

        self.__image_pieces = pygame.image.load(
            path_join("ui", "data", "graphic", "pieces.png"))

        x = int()
        for piece in sorted(self.__pieces_position.keys()):
            self.__images[piece] = self.__image_pieces.subsurface(pygame.Rect(
                x, 0, Data.Pixel.BOARD, Data.Pixel.BOARD))

            x += Data.Pixel.BOARD

        # ----------------------------------------
        #   Resources - Animals
        # ----------------------------------------

        self.__image_animals = pygame.image.load(
            path_join("ui", "data", "graphic", "animals.png"))

        x = int()
        for animal in sorted(self.__animals.keys()):
            self.__images[animal] = self.__image_animals.subsurface(pygame.Rect(
                x, 0, Data.Pixel.CASE, Data.Pixel.CASE))

            x += Data.Pixel.CASE

        # ----------------------------------------
        #   Resources - Volume
        # ----------------------------------------

        self.__image_volume = pygame.image.load(
            path_join("ui", "data", "graphic", "volume.png"))

        x = int()
        for volume in [ "volume", "volume-mute" ]:
            self.__images[volume] = self.__image_volume.subsurface(pygame.Rect(
                x, 0, Data.Pixel.SIDEBAR_ICON, Data.Pixel.SIDEBAR_ICON))

            x += Data.Pixel.SIDEBAR_ICON


    def __exit_pygame(self):
        """ Close engine
        """

        pygame.quit()


    def __manage_events(self):
        """ Manage pygame events
        """

        for event in pygame.event.get():

            # Close window
            if event.type == QUIT:
                self.__is_alive = False

            elif event.type == KEYDOWN:

                if event.key == K_F11:
                    self.__on_check_boards()

                if event.key == K_F12:
                    self.__on_auto_resolve_boards()

            # Mouse button click
            elif event.type == MOUSEBUTTONUP:

                # Mouse left button
                if event.button == 1:
                    self.__on_left_mouse_button(event)

                # Mouse right button
                elif event.button == 3:
                    self.__on_right_mouse_button(event)

            # Mouse motion
            elif event.type == MOUSEMOTION:

                # Update screen when mouse move in drag mode
                if self.__mouse_drag_mode:
                    self.__need_to_update = True


    def __manage_graphics(self):
        """ Manage graphics events
        """

        # Refresh pygame display
        if self.__need_to_update:
            self.__need_to_update = False

            # Draw background color on screen
            self.__screen.blit(self.__background, (0, 0))

            self.__background.fill(self.BACKGROUND)

            # Draw the sidebar with animals score
            self.__draw_sidebar()

            # Draw the four boards with animals
            self.__draw_board()

            # Draw the check buttons
            self.__draw_buttons()

            # Draw available pieces
            self.__draw_pieces()
            self.__draw_pieces_selector()

            # Draw warning message
            self.__draw_warning()

            # Draw current selected piece
            self.__draw_selected_piece()

            # Refresh screen
            pygame.display.flip()


    def __draw_sidebar(self):
        """ Draw sidebar
        """

        width, height = self.get_size()

        # Draw sidebar background
        pygame.draw.rect(self.__screen, self.SIDEBAR, pygame.Rect(
            0, 0, width, Data.Pixel.SIDEBAR_ICON))

        # Volume icon
        image = "volume"
        if self.__mute:
            image = "volume-mute"

        # Draw volume icon
        self.__screen.blit(self.__images[image],
            (width - Data.Pixel.SIDEBAR_ICON, 0))

        # Store volume icon position
        self.__volume_position = pygame.Rect(width - Data.Pixel.SIDEBAR_ICON, 0,
            Data.Pixel.SIDEBAR_ICON, Data.Pixel.SIDEBAR_ICON)

        index = 0
        for animal in self.__animals.keys():

            # Calc sidebar case horizontal position
            x = (index * (Data.Pixel.BORDER + 48)) + (
                index * Data.Pixel.SIDEBAR_ICON)

            self.__sidebar_position[animal] = pygame.Rect(
                x, 0, Data.Pixel.SIDEBAR_ICON + 18, Data.Pixel.SIDEBAR_ICON)

            # Draw animal icon
            self.__screen.blit(pygame.transform.scale(self.__images[animal],
                (Data.Pixel.SIDEBAR_ICON, Data.Pixel.SIDEBAR_ICON)), (x, 0))

            # Draw animal number
            self.__screen.blit(self.__font.render(
                str(self.__animals[animal]), True, self.FONT),
                (x + Data.Pixel.SIDEBAR_ICON, 14))

            index += 1


    def __draw_board(self):
        """ Draw boards
        """

        for position in self.__boards_position.keys():
            x, y = position

            board = (x * 1) + (y * 2)

            x = Data.Pixel.BORDER + (x * (
                (3 * Data.Pixel.CASE) + 2)) + (x * Data.Pixel.BORDER)
            y = Data.Pixel.SIDEBAR_ICON + Data.Pixel.BORDER + (y * (
                (3 * Data.Pixel.CASE) + 2)) + (y * 10)

            # Store board coordinate area
            self.__boards_position[position] = pygame.Rect(
                x, y, Data.Pixel.BOARD, Data.Pixel.BOARD)

            for row in range(0, 3):

                for column in range(0, 3):

                    pygame.draw.rect(self.__screen, self.__message_color,
                        pygame.Rect(
                            x + (column * (Data.Pixel.CASE + 1)),
                            y + (row * (Data.Pixel.CASE + 1)),
                            Data.Pixel.CASE, Data.Pixel.CASE))

                    image = Data.Animal.POSITION[board][row][column]

                    if image is not None:
                        self.__screen.blit(self.__images[image],
                            (x + (column * (Data.Pixel.CASE + 1)),
                            y + (row * (Data.Pixel.CASE + 1))))


    def __draw_pieces(self):
        """ Draws pieces on board
        """

        for piece, values in self.__pieces.items():
            position = values["position"]

            if position is not None:
                x, y = position

                x = Data.Pixel.BORDER + (x * (
                    (3 * Data.Pixel.CASE) + 2)) + (x * Data.Pixel.BORDER)
                y = Data.Pixel.SIDEBAR_ICON + Data.Pixel.BORDER + (y * (
                    (3 * Data.Pixel.CASE) + 2)) + (y * 10)

                self.__screen.blit(pygame.transform.rotate(
                    self.__images[piece], values["rotation"]), (x, y))


    def __draw_pieces_selector(self):
        """ Draw the pieces under the boards
        """

        # Calc piece horizontal position
        x = (3 * Data.Pixel.BORDER) + (2 * Data.Pixel.BOARD)

        index = 0
        for piece, values in self.__pieces.items():

            # Calc piece vertical position
            y = Data.Pixel.SIDEBAR_ICON + Data.Pixel.BORDER + (
                index * Data.Pixel.BORDER) + (index * Data.Pixel.PIECE)

            # Piece not used
            if values["position"] is None and \
                not self.__selected_piece == piece:

                # Draw piece
                self.__screen.blit(pygame.transform.scale(self.__images[piece],
                    (Data.Pixel.PIECE, Data.Pixel.PIECE)), (x, y))

                # Store pieces coordinate area
                self.__pieces_position[piece] = pygame.Rect(
                    x, y, Data.Pixel.PIECE, Data.Pixel.PIECE)

            # Piece used
            else:
                self.__pieces_position[piece] = None

            index += 1


    def __draw_selected_piece(self):
        """ Draw selected piece under mouse cursor
        """

        # Draw only in drag mode with a selected piece
        if self.__selected_piece is not None and self.__mouse_drag_mode:
            x, y = pygame.mouse.get_pos()

            # Draw animal icon
            self.__screen.blit(pygame.transform.rotate(pygame.transform.scale(
                self.__images[self.__selected_piece], (64, 64)),
                self.__selected_piece_rotation), (x, y))


    def __draw_warning(self):
        """ Draw warning message
        """

        if self.__message is not None:

            x = divmod(self.__size[0], 2)[0]

            # Calc button vertical position
            y = Data.Pixel.SIDEBAR_ICON + (3 * Data.Pixel.BORDER) + (
                2 * Data.Pixel.BOARD) + (Data.Pixel.BORDER / 2)

            render = self.__font_half.render(self.__message, True, self.FONT)

            self.__screen.blit(render,
                (x - divmod(render.get_rect().width, 2)[0], y))


    def __draw_buttons(self):
        """ Draw buttons
        """

        x, y = self.__size

        for key, values in self.__buttons_position.items():

            render = self.__font_medium.render(values["text"], True, self.FONT)
            area = render.get_rect()

            area.width += 20
            area.height += 20
            area.x = x - area.width - 10
            area.y = y - area.height - 10

            pygame.draw.rect(self.__screen, self.SIDEBAR, area)

            self.__screen.blit(render, (area.x + 10, area.y + 10))

            values["position"] = area

            x -= area.width + 10


    def __on_left_mouse_button(self, event):
        """ Manage left button events

        Parameters
        ----------
        event : pygame.event.Event
            The catch event
        """

        self.__warning = False

        selection = None

        # ----------------------------------------
        #   Select mode - Click on a specific area
        # ----------------------------------------

        if not self.__mouse_drag_mode:

            # ----------------------------------------
            #   Volume icon
            # ----------------------------------------

            if self.__volume_position is not None:

                if self.__volume_position.collidepoint(event.pos):
                    self.__mute = not self.__mute

                    if self.__mute:
                        pygame.mixer.music.pause()
                    else:
                        pygame.mixer.music.unpause()

                    self.__need_to_update = True

            # ----------------------------------------
            #   Sidebar icon
            # ----------------------------------------

            for animal, position in self.__sidebar_position.items():

                # User click on a specific animal in sidebar
                if position is not None and position.collidepoint(event.pos):
                    self.__animals[animal] += 1

                    if self.__animals[animal] > Data.Animal.MAXIMUM[animal]:
                        self.__animals[animal] = Data.Animal.MAXIMUM[animal]

                    self.__need_to_update = True

            # ----------------------------------------
            #   Buttons
            # ----------------------------------------

            for button, values in self.__buttons_position.items():
                position = values["position"]

                # User click on a specific button
                if position is not None and position.collidepoint(event.pos):
                    method = values["method"]

                    if method is not None:
                        method()

            # ----------------------------------------
            #   Board
            # ----------------------------------------

            for board, position in self.__boards_position.items():

                # User click on a specific board
                if position is not None and position.collidepoint(event.pos):
                    selection = board
                    break

            # ----------------------------------------
            #   Piece
            # ----------------------------------------

            for piece, position in self.__pieces_position.items():

                # User click on a specific piece
                if position is not None and position.collidepoint(event.pos):
                    selection = piece
                    break

            # ----------------------------------------
            #   Select a board or a piece
            # ----------------------------------------

            if selection is not None:

                # Board
                if type(selection) is tuple:

                    # Check if a piece was set to this board
                    for piece, values in self.__pieces.items():

                        if values["position"] == selection:
                            self.__pieces[piece]["position"] = None

                            self.__selected_piece = piece
                            self.__selected_piece_rotation = \
                                self.__pieces[piece]["rotation"]

                            self.__mouse_drag_mode = True
                            self.__need_to_update = True

                # Piece
                elif type(selection) is str:
                    self.__selected_piece = selection
                    self.__selected_piece_rotation = Data.Rotation.NORTH

                    self.__mouse_drag_mode = True
                    self.__need_to_update = True

        # ----------------------------------------
        #   Drag mode - Put piece on screen
        # ----------------------------------------

        else:

            # ----------------------------------------
            #   Board
            # ----------------------------------------

            for board, position in self.__boards_position.items():
                # User click on a specific board
                if position is not None and position.collidepoint(event.pos):
                    selection = board
                    break

            # ----------------------------------------
            #   Select a board
            # ----------------------------------------

            if selection is not None:
                current_piece = None

                for piece, values in self.__pieces.items():
                    if values["position"] == selection:
                        current_piece = piece

                # ----------------------------------------
                #   Select an empty board
                # ----------------------------------------

                if current_piece is None:
                    self.__pieces[self.__selected_piece]["position"] = selection
                    self.__pieces[self.__selected_piece]["rotation"] = \
                        self.__selected_piece_rotation

                    self.__selected_piece = None

                    self.__mouse_drag_mode = False
                    self.__need_to_update = True

                # ----------------------------------------
                #   Select a not empty board
                # ----------------------------------------

                else:
                    self.__pieces[current_piece]["position"] = None
                    self.__pieces[current_piece]["rotation"] = \
                        Data.Rotation.NORTH

                    self.__pieces[self.__selected_piece]["position"] = selection
                    self.__pieces[self.__selected_piece]["rotation"] = \
                        self.__selected_piece_rotation

                    self.__selected_piece = None

                    self.__mouse_drag_mode = False
                    self.__need_to_update = True

            # ----------------------------------------
            #   Click outside a board
            # ----------------------------------------

            else:
                self.__selected_piece = None

                self.__mouse_drag_mode = False
                self.__need_to_update = True


    def __on_right_mouse_button(self, event):
        """ Manage right button events

        Parameters
        ----------
        event : pygame.event.Event
            The catch event
        """

        self.__warning = False

        # ----------------------------------------
        #   Drag mode - Rotate selected piece
        # ----------------------------------------

        if self.__mouse_drag_mode:
            rotations = [
                Data.Rotation.NORTH,
                Data.Rotation.EAST,
                Data.Rotation.SOUTH,
                Data.Rotation.WEST
            ]

            # Switch to the next rotation
            rotation = rotations.index(self.__selected_piece_rotation) - 1
            # Loop back to the last one
            if rotation < 0:
                rotation = len(rotations) - 1

            self.__selected_piece_rotation = rotations[rotation]

            self.__need_to_update = True

        # ----------------------------------------
        #   Select mode - Decrease sidebar case
        # ----------------------------------------

        else:

            for animal, position in self.__sidebar_position.items():

                # User click on a specific animal in sidebar
                if position is not None and position.collidepoint(event.pos):
                    self.__animals[animal] -= 1

                    if self.__animals[animal] < 0:
                        self.__animals[animal] = 0

                    self.__need_to_update = True


    def __on_generate_data(self):
        """
        """

        pieces = list()

        for piece in sorted(self.__pieces.keys()):

            # Retrieve piece rotation
            rotation, result = divmod(self.__pieces[piece]["rotation"], 90)

            # Retrieve piece position
            value = self.__pieces[piece]["position"]
            if value is not None:
                value = (value[0] * 1) + (value[1] * 2)

            pieces.append([ rotation, value ])

        # Calc the value from animals numbers
        value = self.__animals[Data.Animal.LION] * DataSolver.LION
        value += self.__animals[Data.Animal.ZEBRA] * DataSolver.ZEBRA
        value += self.__animals[Data.Animal.ANTELOPE] * DataSolver.ANTELOPE
        value += self.__animals[Data.Animal.ELEPHANT] * DataSolver.ELEPHANT
        value += self.__animals[Data.Animal.RHINOCEROS] * DataSolver.RHINOCEROS

        return (pieces, value)


    def __on_check_boards(self):
        """ Check board if current pieces position can resolve puzzle
        """

        self.__message = None
        self.__message_color = self.BOARD

        pieces, value = self.__on_generate_data()

        # Generate a new solver
        solver = Solver()
        solver.setPieces(pieces)

        result = solver.solve(value)

        if result is None:
            self.__message = "Cannot resolve puzzle with current position"
            self.__message_color = self.WARNING

        else:
            self.__message = "GREAT JOB !!"
            self.__message_color = self.GOOD

            for piece, values in self.__pieces.items():

                if values["position"] is None:
                    self.__message = "Almost !"
                    self.__message_color = self.BOARD
                    break

        self.__need_to_update = True


    def __on_auto_resolve_boards(self):
        """ Auto resolve puzzle
        """

        self.__message = None
        self.__message_color = self.BOARD

        pieces, value = self.__on_generate_data()

        # Generate a new solver
        solver = Solver()
        solver.setPieces(pieces)

        result = solver.solve(value)

        if result is not None:
            index = int()

            while index < len(result):
                board, rest = divmod(index, 2)

                piece = "piece_%d" % (result[index][1] + 1)

                self.__pieces[piece]["rotation"] = result[index + 1][1] * 90
                self.__pieces[piece]["position"] = \
                    list(self.__boards_position.keys())[board]

                index += 2

            self.__message = "Auto-resolve done"

        else:

            for piece, values in self.__pieces.items():
                values["rotation"] = Data.Rotation.NORTH
                values["position"] = None

        self.__need_to_update = True


    def run(self):
        """ Start main loop
        """

        # Check pygame display module
        if not pygame.display.get_init():
            raise ImportError("PyGame display has not been initialized")

        # Manage fullscreen mode
        if self.__fullscreen:
            self.toggle_fullscreen()

        # Start music
        pygame.mixer.music.play(loops=-1)

        # Pause the music if mute mode is activate
        if self.__mute:
            pygame.mixer.music.pause()

        # ----------------------------------------
        #   Main loop
        # ----------------------------------------

        while self.__is_alive:

            # Block display to a specific frames per second value
            self.__clock.tick(self.FPS)

            # Manage pygame events
            self.__manage_events()

            # Manage pygame graphics
            self.__manage_graphics()

        self.__exit_pygame()


    # ----------------------------------------
    #   Getter/Setter
    # ----------------------------------------

    def get_size(self):
        """ Retrieve current screen size

        Returns
        -------
        tuple
            Window size as int tuple
        """

        return self.__size


    def is_fullscreen(self):
        """ Retrieve current fullscreen mode

        Returns
        -------
        bool
            Window fullscreen mode
        """

        return self.__fullscreen


    def set_size(self, width, height):
        """ Set the screen size

        Parameters
        ----------
        width : int
            Window width size
        height : int
            Window height size

        Raises
        ------
        TypeError
            When width type is not int
        TypeError
            When height type is not int
        """

        if type(width) is not int:
            raise TypeError("Wrong type for width, expected int")

        if type(height) is not int:
            raise TypeError("Wrong type for height, expected int")

        self.__size = (width, height)


    def toggle_fullscreen(self):
        """ Switch between fullscreen mode
        """

        pygame.display.toggle_fullscreen()
