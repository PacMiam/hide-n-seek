# ------------------------------------------------------------------------------
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
#   Class
# ------------------------------------------------------------------------------

class Data(object):

    NAME = "Hide & Seek Safari"
    VERSION = "0.1"
    COPYLEFT = "Copyleft 2017 Lebot/Lubert"


    class Pixel(object):

        SCREEN_WIDTH = 748
        SCREEN_HEIGHT = 780

        BORDER = 10

        CASE = 96
        BOARD = 290
        PIECE = 128

        CURSOR = 64
        MEDIUM = 48
        HALF = 32

        SIDEBAR_ICON = 64


    class Piece(object):

        FIRST = "piece_1"
        SECOND = "piece_2"
        THIRD = "piece_3"
        FOURTH = "piece_4"


    class Animal(object):

        LION = "lion"
        ZEBRA = "zebra"
        ANTELOPE = "antelope"
        ELEPHANT = "elephant"
        RHINOCEROS = "rhinoceros"

        POSITION = [
            [
                [ ELEPHANT, None, LION ],
                [ ZEBRA, ANTELOPE, ZEBRA ],
                [ RHINOCEROS, LION, ELEPHANT ],
            ],
            [
                [ ANTELOPE, None, LION ],
                [ None, ZEBRA, ELEPHANT ],
                [ LION, RHINOCEROS, ANTELOPE ],
            ],
            [
                [ RHINOCEROS, ELEPHANT, ZEBRA ],
                [ ANTELOPE, None, ANTELOPE ],
                [ LION, None, RHINOCEROS ],
            ],
            [
                [ None, None, None ],
                [ None, RHINOCEROS, LION ],
                [ ANTELOPE, ELEPHANT, ZEBRA ],
            ],
        ]

        MAXIMUM = {
            LION: 6,
            ZEBRA: 5,
            ANTELOPE: 6,
            ELEPHANT: 5,
            RHINOCEROS: 5,
        }


    class Board(object):

        TOPLEFT = (0, 0)
        TOPRIGHT = (1, 0)
        BOTTOMLEFT = (0, 1)
        BOTTOMRIGHT = (1, 1)


    class Rotation(object):

        NORTH = 0
        EAST = 90
        SOUTH = 180
        WEST = 270
