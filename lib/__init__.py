# ------------------------------------------------------------------------------
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-130(0,0), USA.
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
#   Class
# ------------------------------------------------------------------------------

class Data(object):

    INPUTVAL = 0

    LION = 1000
    ZEBRA = 10000
    ANTELOPE = 10
    ELEPHANT = 1
    RHINOCEROS = 100

    POSITION = [
            [
                [ ELEPHANT, 0, LION ],
                [ ZEBRA, ANTELOPE, ZEBRA ],
                [ RHINOCEROS, LION, ELEPHANT ],
            ],
            [
                [ ANTELOPE, 0, LION ],
                [ 0, ZEBRA, ELEPHANT ],
                [ LION, RHINOCEROS, ANTELOPE ],
            ],
            [
                [ RHINOCEROS, ELEPHANT, ZEBRA ],
                [ ANTELOPE, 0, ANTELOPE ],
                [ LION, 0, RHINOCEROS ],
            ],
            [
                [ 0, 0, 0 ],
                [ 0, RHINOCEROS, LION ],
                [ ANTELOPE, ELEPHANT, ZEBRA ],
            ],
        ]

    NBSEEING = [3,2,2,2]

    ROTPIECE = [
        [
            [ (0,2), (1,0), (2,0), ],
            [ (0,0), (2,1), (2,2), ],
            [ (0,2), (1,2), (2,0), ],
            [ (0,0), (0,1), (2,2), ],
        ],
        [
            [ (1,1), (1,2), (-1,-1), ],
            [ (0,1), (1,1), (-1,-1), ],
            [ (1,0), (1,1), (-1,-1), ],
            [ (1,1), (2,1), (-1,-1), ],
        ],
        [
            [ (0,1), (2,1), (-1,-1), ],
            [ (1,0), (1,2), (-1,-1), ],
            [ (0,1), (2,1), (-1,-1), ],
            [ (1,0), (1,2), (-1,-1), ],
        ],
        [
            [ (0,1), (2,0), (-1,-1), ],
            [ (1,0), (2,2), (-1,-1), ],
            [ (0,2), (2,1), (-1,-1), ],
            [ (0,0), (1,2), (-1,-1), ],
        ],
    ]


