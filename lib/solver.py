# ------------------------------------------------------------------------------
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
#   Modules - System
# ------------------------------------------------------------------------------

# System
from sys import exit as sys_exit

# ------------------------------------------------------------------------------
#   Modules - Solver
# ------------------------------------------------------------------------------

from lib import Data

# ------------------------------------------------------------------------------
#   Modules - Constraint
# ------------------------------------------------------------------------------

try:
    from constraint import *

except ImportError as error:
    sys_exit("Cannot import python3-constraint module: %s" % str(error))

# ------------------------------------------------------------------------------
#   Modules - operator
# ------------------------------------------------------------------------------

import operator

# ------------------------------------------------------------------------------
#   Class
# ------------------------------------------------------------------------------

class Solver(object):
    """
    """

    def __init__(self):
        """ Constructor
        """

        # ----------------------------------------
        #   Variables
        # ----------------------------------------

        self.__inputval = 4

        self.__affect = range(8)

        self.__definedPieces = [
                            [-1, None,],
                            [-1, None,],
                            [-1, None,],
                            [-1, None,],
                        ]

        self.__problem = Problem()

        # ----------------------------------------
        #   Variables Addition To Problem
        # ----------------------------------------

        self.__problem.addVariables(self.__affect, range(0, 4))

        # ----------------------------------------
        #   Contraints Definition
        # ----------------------------------------

        self.__problem.addConstraint(lambda p1, r1, p2, r2, p3, r3, p4, r4 :
            self.customAllPiecesDifferent(p1, r1, p2, r2, p3, r3, p4, r4))

        self.__problem.addConstraint(lambda p1, r1, p2, r2, p3, r3, p4, r4 :
            self.customSumContraints(p1, r1, p2, r2, p3, r3, p4, r4))

        self.__problem.addConstraint(lambda p1, r1, p2, r2, p3, r3, p4, r4 :
            self.customAlreadySetPieces(p1, r1, p2, r2, p3, r3, p4, r4))

    # ----------------------------------------
    #   Methods
    # ----------------------------------------

    def setPieces(self,definedPieces):

        self.__definedPieces = definedPieces


    def resetPieces(self):

        self.__definedPieces = [
                            [-1, None,],
                            [-1, None,],
                            [-1, None,],
                            [-1, None,],
                        ]


    def customAllPiecesDifferent(self, p1, r1, p2, r2, p3, r3, p4, r4):
        """
        """

        # Variables init
        seen = set()
        pieces = [p1, p2, p3, p4]

        for i in range(0,4):

            if (pieces[i] in seen):

                return False

            seen.add(pieces[i])

        return True



    def customSumContraints(self, p1, r1, p2, r2, p3, r3, p4, r4):
        """
        """

        # Variables init
        sumVal = 0
        pieces = [p1, p2, p3, p4]
        rots   = [r1 ,r2 ,r3 ,r4]

        #
        for i in range(0,4):

            for j in range(0,Data.NBSEEING[pieces[i]]):

                temp = Data.POSITION[i][
                                Data.ROTPIECE[pieces[i]][rots[i]][j][0]
                            ][
                                Data.ROTPIECE[pieces[i]][rots[i]][j][1]
                            ]

                if temp is not None:

                    sumVal += temp


        return sumVal == self.__inputval


    def customAlreadySetPieces(self, p1, r1, p2, r2, p3, r3, p4, r4):
        """

        definedPieces[
            [rotPiece, numBoard,],
            [rotPiece, numBoard,],
            [rotPiece, numBoard,],
            [rotPiece, numBoard,],

        exemple :
            pieces = [
                [ 2, 0,],
                [-1, None,],
                [-1, None,],
                [-1, None,],
            ]

            solver = Solver()

            solver.setPieces(pieces)

            solver.solve(4)
        ]
        """
        pieces = [p1, p2, p3, p4]
        rots   = [r1 ,r2 ,r3 ,r4]

        for i in range(0,4):

            if self.__definedPieces[i][1] is not None:

                if pieces[self.__definedPieces[i][1]] != i:

                    return False

                if rots[self.__definedPieces[i][1]] != self.__definedPieces[i][0]:

                    return False

        return True


    def solve(self, inputval):
        """
        """

        solutions_sorted = None

        if inputval > 0:
            self.__inputval = inputval

            solutions =  self.__problem.getSolution()

            if solutions is not None:
                solutions_sorted = sorted(
                    solutions.items(), key=operator.itemgetter(0))

        """
        i = 0
        while i < 8:

            print(" Piece: %s  Rot : %s" % (solutions_sorted[i][1], solutions_sorted[i+1][1]))
            i+=2
        """

        return solutions_sorted



