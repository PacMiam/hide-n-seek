# ------------------------------------------------------------------------------
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
#   Modules - System
# ------------------------------------------------------------------------------

# System
from argparse import ArgumentParser

# ------------------------------------------------------------------------------
#   Modules - Engine
# ------------------------------------------------------------------------------

from ui import Data

from ui.engine import Engine

# ------------------------------------------------------------------------------
#   Launcher
# ------------------------------------------------------------------------------

def main():
    """ Main launcher
    """

    # Generate default arguments
    parser = ArgumentParser(description=Data.NAME,
        epilog=Data.COPYLEFT, conflict_handler="resolve")

    parser.add_argument("-v", "--version", action="version",
        version="%s %s - Licence GPLv3" % (Data.NAME, Data.VERSION),
        help="show the current version")

    parser_data = parser.add_argument_group("data arguments")
    parser_data.add_argument("-l", metavar="LION", default=0, type=int,
        action="store", help="number of lions to found")
    parser_data.add_argument("-e", metavar="ELEPHANT", default=0, type=int,
        action="store", help="number of elephant to found")
    parser_data.add_argument("-z", metavar="ZEBRA", default=0, type=int,
        action="store", help="number of zebra to found")
    parser_data.add_argument("-a", metavar="ANTELOPE", default=0, type=int,
        action="store", help="number of antelope to found")
    parser_data.add_argument("-r", metavar="RHINOCEROS", default=0, type=int,
        action="store", help="number of rhinoceros to found")

    parser_sound = parser.add_argument_group("sound arguments")
    parser_sound.add_argument("-V", "--volume", default=0.1, type=float,
        action="store", help="set volume level for music background")
    parser_sound.add_argument("-M", "--mute",
        action="store_true", help="set mute mode for music background")

    args = parser.parse_args()

    # ----------------------------------------
    #   Engine
    # ----------------------------------------

    Engine(args).run()


if __name__ == "__main__":
    main()
